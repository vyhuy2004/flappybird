#pragma once
#include "led_matrix.h"

void draw_bird(uint8_t x, uint8_t y);
void draw_flappy(void);
void clear_bird(uint8_t x, uint8_t y);
void draw_obs(uint8_t speed);
bool check_collision(int x, int y);
void draw_score_bottom_template(void);
void draw_score(int score, int x, int y);
void draw_end_screen(int score);
void clear_obsbuff(uint8_t color);