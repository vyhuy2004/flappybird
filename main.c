#include "FreeRTOS.h"
//#include "adc.h"
#include "board_io.h"
#include "common_macros.h"
#include "delay.h"
#include "game_graphic.h"
#include "gpio.h"
#include "led_matrix.h"
#include "periodic_scheduler.h"
#include "semphr.h"
#include "task.h"
#include "time.h"
#include <stdio.h>
static void start_screen_task(void *param);
static void bird_falling_task(void *param);
static void speed_control_task(void *param);
static void end_screen_task(void *param);

static xTaskHandle start_handle;
static xTaskHandle bird_falling_handle;
static xTaskHandle end_handle;

uint8_t speed = 4;

gpio_s sw3;

static volatile int bird_location;
static volatile int bird_delay;
static volatile bool bird_jump;
static volatile int score = 0;
static volatile int state = 0;

int main(void) {
  // adc__initialize();
  srand(time(NULL));
  led_matrix_init();

  sw3 = board_io__get_sw3();

  clear_screen(0);

  xTaskCreate(start_screen_task, "start_screen", 2048 / sizeof(void *), NULL, 3, &start_handle);
  xTaskCreate(bird_falling_task, "bird_falling", 4096 / sizeof(void *), NULL, 3, &bird_falling_handle);
  xTaskCreate(end_screen_task, "end_screen", 2048 / sizeof(void *), NULL, 3, &end_handle);
  xTaskCreate(speed_control_task, "spd", 2048 / sizeof(void *), NULL, 4, NULL);

  vTaskStartScheduler();

  return 0;
}

void start_screen_task(void *param) {
  int location = 14;
  int bird_speed = 3;
  while (true) {
    if (state == 0) {
      draw_flappy();
      draw_bird(location, 25);
      update_Display();
      clear_bird(location, 25);
      if (bird_speed == 0) {
        if (location == 14) {
          location = 18;
        } else {
          location = 14;
        }
        bird_speed = 3;
      } else {
        bird_speed--;
      }

      vTaskDelay(20);
    }
  }
}

void bird_falling_task(void *param) {
  bird_location = 0;
  bird_delay = 6;
  score = 0;
  while (true) {
    if (state != 1) {
      clear_screen(0);
      update_Display();
      vTaskSuspend(NULL);
    }

    draw_score_bottom_template();
    draw_obs(speed);
    draw_bird(bird_location, 2);
    update_Display();

    clear_bird(bird_location, 2);

    if (bird_delay == 0) {
      // if collide do sth
      if (check_collision(bird_location, 2)) {
        state = 2;
        // continue;
      } else if (check_buffer_color(2)) {
        score = score + 1;
        fprintf(stderr, "Score:%u\n", score);
      }
      draw_score(score, 28, 27);
      if (bird_jump == true && bird_location != 0) {
        bird_location = bird_location - 1;
        bird_jump = false;
      } else if (bird_location < 22) {
        bird_location += 1;
      }
      bird_delay = 6;
    } else {
      bird_delay--;
    }

    vTaskDelay(20);
  }
}

void speed_control_task(void *param) {
  bird_jump = false;

  while (true) {
    switch (state) {
    case 0:
      if (gpio__get(sw3)) {

        clear_screen(0);
        update_Display();
        vTaskDelay(20);
        state = 1;
        vTaskResume(bird_falling_handle);
        clear_obsbuff(0);
      }
      break;
    case 1:
      if (gpio__get(sw3) && bird_location > 0) {
        bird_jump = true;
      }
      break;
    case 2:
      if (gpio__get(sw3)) {

        score = 0;
        bird_location = 0;
        clear_screen(0);
        update_Display();
        state = 0;
        // vTaskSuspend(end_handle);
        vTaskDelay(20);
        // vTaskResume(start_handle);
      }
      break;
    }

    vTaskDelay(100);
  }
}

void end_screen_task(void *param) {

  while (true) {
    if (state == 2) {
      draw_end_screen(score);
      update_Display();
      vTaskDelay(20);
    }
  }
}
