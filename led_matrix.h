#pragma once
#include "gpio.h"

void led_matrix_init(void);
void enable_OE(void);
void disable_OE(void);
void enable_LAT(void);
void disable_LAT(void);
void set_Row(uint8_t row);
void set_Pixel_buffer(uint8_t row, uint8_t column, uint8_t color);
void update_Display(void);
// void draw_bord();
void clear_screen(uint8_t color);
void add_matrix_to_buffer(uint8_t x, uint8_t y, uint8_t no_of_row, uint8_t no_of_col, uint8_t matrix[][no_of_col]);
bool check_buffer_color(uint8_t color);