#include "game_graphic.h"
#include <stdlib.h>

#define LED_BLACK 0
#define LED_RED 1
#define LED_GREEN 2
#define LED_YELLOW 3
#define LED_BLUE 4
#define LED_MAGENTA 5
#define LED_CYAN 6
#define LED_WHITE 7
#define LED_MATRIX_W 64
#define LED_MATRIX_H 28
#define OBS_W 4 // width of the obs
#define OBS_H LED_MATRIX_H
#define OBS_SPACING 15 // space between obs
#define OBS_COLOR LED_GREEN
#define OBS_SPEED 5 // 0: default, 1: slowest, ...6: fastest

static uint8_t obsbuff[32][64];
// static uint32_t obs_speed[] = {30, 50, 40, 30, 20, 10, 0};
static uint32_t obs_speed[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

typedef uint8_t small_font_t[4][3];

// static uint8_t bird_matrix[10][17] = {
//     {0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0}, {0, 0, 7, 0, 0, 7, 6, 6, 0, 7, 7, 7, 7, 0, 7, 0, 0},
//     {7, 0, 0, 0, 0, 6, 6, 6, 0, 7, 7, 7, 0, 7, 0, 7, 0}, {0, 7, 7, 7, 7, 0, 6, 6, 0, 7, 7, 7, 0, 7, 0, 7, 0},
//     {0, 7, 7, 7, 7, 7, 0, 6, 6, 0, 7, 7, 7, 7, 0, 7, 0}, {0, 6, 7, 7, 7, 6, 0, 6, 6, 6, 0, 0, 0, 0, 0, 0, 7},
//     {7, 0, 6, 6, 6, 0, 6, 6, 6, 0, 1, 1, 1, 1, 1, 1, 0}, {0, 7, 0, 0, 0, 4, 4, 4, 0, 1, 0, 0, 0, 0, 0, 0, 7},
//     {0, 7, 0, 4, 4, 4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 0, 7}, {0, 0, 7, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 7, 0},
// };

// static uint8_t bird_clear_matrix[10][17] = {
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
// };

static uint8_t bird_matrix[6][11] = {
    {0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0}, {0, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0}, {0, 3, 4, 0, 7, 7, 7, 0, 4, 3, 0},
    {3, 3, 4, 7, 7, 1, 7, 7, 4, 3, 3}, {0, 3, 4, 7, 7, 3, 7, 7, 4, 3, 0}, {0, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0},
};

static uint8_t bird_clear_matrix[6][11] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

static uint8_t f_matrix[6][6] = {
    {1, 1, 1, 1, 1, 1}, {1, 1, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1},
    {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0},
};

static uint8_t l_matrix[6][6] = {
    {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1},
};

static uint8_t a_matrix[6][6] = {
    {0, 0, 1, 1, 0, 0}, {0, 1, 0, 0, 1, 0}, {1, 1, 0, 0, 1, 1},
    {1, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 1},
};

static uint8_t p_matrix[6][6] = {
    {1, 1, 1, 1, 1, 0}, {1, 1, 0, 0, 0, 1}, {1, 1, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 0}, {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0},
};

static uint8_t y_matrix[6][6] = {
    {1, 0, 0, 0, 0, 1}, {1, 1, 0, 0, 1, 1}, {0, 1, 1, 1, 1, 0},
    {0, 0, 1, 1, 0, 0}, {0, 0, 1, 1, 0, 0}, {0, 0, 1, 1, 0, 0},
};

static small_font_t s_small_matrix = {
    {0, 7, 7},
    {7, 7, 0},
    {0, 0, 7},
    {7, 7, 0},
};
static small_font_t c_small_matrix = {
    {7, 7, 7},
    {7, 0, 0},
    {7, 0, 0},
    {7, 7, 7},
};
static small_font_t o_small_matrix = {
    {7, 7, 7},
    {7, 0, 7},
    {7, 0, 7},
    {7, 7, 7},
};
static small_font_t r_small_matrix = {
    {7, 7, 7},
    {7, 0, 7},
    {7, 7, 0},
    {7, 0, 7},
};
static small_font_t e_small_matrix = {
    {7, 7, 7},
    {7, 0, 7},
    {7, 7, 0},
    {0, 7, 7},
};

static small_font_t zero_small_matrix = {
    {1, 1, 1},
    {1, 0, 1},
    {1, 0, 1},
    {1, 1, 1},
};

static small_font_t one_small_matrix = {
    {0, 1, 0},
    {1, 1, 0},
    {0, 1, 0},
    {0, 1, 0},
};

static small_font_t two_small_matrix = {
    {1, 1, 1},
    {0, 0, 1},
    {0, 1, 0},
    {1, 1, 1},
};

static small_font_t three_small_matrix = {
    {1, 1, 1},
    {0, 1, 1},
    {0, 0, 1},
    {1, 1, 1},
};

static small_font_t four_small_matrix = {
    {1, 0, 1},
    {1, 0, 1},
    {1, 1, 1},
    {0, 0, 1},
};

static small_font_t five_small_matrix = {
    {1, 1, 1},
    {1, 0, 0},
    {1, 1, 1},
    {0, 1, 1},
};

static small_font_t six_small_matrix = {
    {1, 0, 0},
    {1, 1, 1},
    {1, 0, 1},
    {1, 1, 1},
};

static small_font_t seven_small_matrix = {
    {1, 1, 1},
    {0, 0, 1},
    {0, 1, 0},
    {1, 0, 0},
};

static small_font_t eight_small_matrix = {
    {0, 1, 1},
    {1, 0, 1},
    {0, 1, 1},
    {1, 1, 0},
};

static small_font_t nine_small_matrix = {
    {1, 1, 1},
    {1, 0, 1},
    {1, 1, 1},
    {0, 0, 1},
};
small_font_t *number_matrix[10] = {&zero_small_matrix,  &one_small_matrix,  &two_small_matrix, &three_small_matrix,
                                   &four_small_matrix,  &five_small_matrix, &six_small_matrix, &seven_small_matrix,
                                   &eight_small_matrix, &nine_small_matrix};

static uint8_t bottom_line_matrix[1][64] = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                             1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                             1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};

uint8_t obs_matrix[OBS_H][OBS_W];

struct {
  uint8_t hi_pos;
  uint8_t lo_pos;
} obs_hole_pos[13] = {{5, 16},  {8, 18}, {11, 21}, {12, 23}, {3, 18},  {7, 22}, {2, 10},
                      {15, 24}, {3, 11}, {10, 20}, {11, 20}, {15, 24}, {17, 26}}; // row number, 0..31

void draw_bird(uint8_t x, uint8_t y) { add_matrix_to_buffer(x, y, 6, 11, bird_matrix); }

void create_obs() {
  uint8_t hole_hi, hole_lo;
  uint8_t row, col;
  uint8_t pos;

  for (row = 0; row < OBS_H; row++)
    for (col = 0; col < OBS_W; col++)
      obs_matrix[row][col] = OBS_COLOR;

  pos = (uint8_t)(rand() % 13);

  hole_hi = obs_hole_pos[pos].hi_pos;
  hole_lo = obs_hole_pos[pos].lo_pos;
  for (row = hole_hi; row <= hole_lo; row++)
    for (col = 0; col < OBS_W; col++)
      obs_matrix[row][col] = LED_BLACK;
}

void shift_obsbuff_left() {
  int col, row;

  for (col = 1; col < 64; col++) {
    for (row = 0; row < 32; row++) {
      obsbuff[row][col - 1] = obsbuff[row][col];
    }
  }

  // Clear last column
  for (row = 0; row < 32; row++)
    obsbuff[row][63] = 0;
}

void clear_obsbuff(uint8_t color) {
  for (int row = 0; row < 32; row++)
    for (int col = 0; col < 64; col++)
      obsbuff[row][col] = color;
}

void draw_obs(uint8_t speed) {
  uint8_t row;
  static uint8_t obs_state = 0; // Initial state
  static uint8_t delay = 0;
  static uint8_t obs_col = 0;
  static uint32_t idle_count;

  switch (obs_state) {
  case 0:
    create_obs();
    obs_col = 0;
    clear_obsbuff(LED_BLACK);
    for (row = 0; row < LED_MATRIX_H; row++)
      obsbuff[row][LED_MATRIX_W - 1] = obs_matrix[row][obs_col];
    obs_col++;
    idle_count = obs_speed[speed];
    obs_state = 1;
    break;
  case 1: // new obs shift-in state
    if (idle_count) {
      idle_count--;
      break;
    }
    idle_count = obs_speed[speed];
    shift_obsbuff_left();
    for (row = 0; row < LED_MATRIX_H; row++)
      obsbuff[row][LED_MATRIX_W - 1] = obs_matrix[row][obs_col];
    obs_col++;
    if (obs_col == OBS_W) {
      delay = 0;
      obs_state = 2; // OBS wait state
    }
    break;
  case 2: // obs wait state
    if (idle_count) {
      idle_count--;
      break;
    }
    idle_count = obs_speed[speed];
    shift_obsbuff_left();
    delay++;
    if (delay == OBS_SPACING) {
      create_obs();
      obs_col = 0;
      obs_state = 1;
    }
    break;
  } // switch

  add_matrix_to_buffer(0, 0, 27, 64, obsbuff);
}

void draw_flappy() {
  add_matrix_to_buffer(6, 12, 6, 6, f_matrix);
  add_matrix_to_buffer(6, 20, 6, 6, l_matrix);
  add_matrix_to_buffer(6, 28, 6, 6, a_matrix);
  add_matrix_to_buffer(6, 36, 6, 6, p_matrix);
  add_matrix_to_buffer(6, 44, 6, 6, p_matrix);
  add_matrix_to_buffer(6, 52, 6, 6, y_matrix);
}
void clear_bird(uint8_t x, uint8_t y) { add_matrix_to_buffer(x, y, 6, 11, bird_clear_matrix); }

bool check_collision(int x, int y) {
  for (int row = x; row < (x + 6); row++) {
    if (row == (x + 0)) {
      if (obsbuff[row][y + 4] != 0 || obsbuff[row][y + 5] != 0 || obsbuff[row][y + 6] != 0) {
        return true;
        break;
      }
    } else if (row == (x + 1)) {
      if (obsbuff[row][y + 2] != 0 || obsbuff[row][y + 3] != 0 || obsbuff[row][y + 7] != 0 ||
          obsbuff[row][y + 8] != 0) {
        return true;
        break;
      }
    } else if (row == (x + 2)) {
      if (obsbuff[row][y + 1] != 0 || obsbuff[row][y + 9] != 0) {
        return true;
        break;
      }
    } else if (row == (x + 3)) {
      if (obsbuff[row][y + 0] != 0 || obsbuff[row][y + 10] != 0) {
        return true;
        break;
      }
    } else if (row == (x + 4)) {
      if (obsbuff[row][y + 1] != 0 || obsbuff[row][y + 9] != 0) {
        return true;
        break;
      }
    } else if (row == (x + 5)) {
      if (obsbuff[row][y + 2] != 0 || obsbuff[row][y + 3] != 0 || obsbuff[row][y + 4] != 0 ||
          obsbuff[row][y + 5] != 0 || obsbuff[row][y + 6] != 0 || obsbuff[row][y + 7] != 0 ||
          obsbuff[row][y + 8] != 0) {
        return true;
        break;
      }
    }
  }
  return false;
}

void draw_score_bottom_template(void) {
  add_matrix_to_buffer(27, 0, 1, 64, bottom_line_matrix);
  add_matrix_to_buffer(28, 2, 4, 3, s_small_matrix);
  add_matrix_to_buffer(28, 7, 4, 3, c_small_matrix);
  add_matrix_to_buffer(28, 12, 4, 3, o_small_matrix);
  add_matrix_to_buffer(28, 17, 4, 3, r_small_matrix);
  add_matrix_to_buffer(28, 22, 4, 3, e_small_matrix);
}

void draw_score(int score, int x, int y) {
  int right_digit = score % 10;
  int left_digit = score / 10;
  add_matrix_to_buffer(x, y, 4, 3, number_matrix[left_digit]);
  add_matrix_to_buffer(x, y + 5, 4, 3, number_matrix[right_digit]);
}

void draw_end_screen(int score) {
  draw_bird(10, 25);
  add_matrix_to_buffer(20, 15, 4, 3, s_small_matrix);
  add_matrix_to_buffer(20, 20, 4, 3, c_small_matrix);
  add_matrix_to_buffer(20, 25, 4, 3, o_small_matrix);
  add_matrix_to_buffer(20, 30, 4, 3, r_small_matrix);
  add_matrix_to_buffer(20, 35, 4, 3, e_small_matrix);
  draw_score(score, 20, 40);
}
